﻿namespace AdventOfCode2022;

public interface IExercise
{
    string R01();
    string R02();
}