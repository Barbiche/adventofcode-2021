﻿using System.Collections;
using System.Text;

namespace AdventOfCode2022.d06;

public class D06 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d06/", "input");

    public string R01()
    {
        var entry     = File.ReadAllText(Input);
        var set       = new HashSet<char>();
        var lastIndex = -1;
        for (var i = 0; i < entry.Length; i++)
        {
            if (set.Contains(entry[i]))
            {
                set.Clear();
                set.Add(entry[i]);
            } else if (set.Count >= 4)
            {
                lastIndex = i-1;
                break;
            }
            else
            {
                set.Add(entry[i]);
            }
        }

        return lastIndex.ToString();
    }

    public string R02()
    {
        var entry     = File.ReadAllText(Input);
        var queue       = new Queue<char>();
        var lastIndex = -1;
        for (var i = 0; i < entry.Length; i++)
        {
            if (queue.Count >= 14)
            {
                lastIndex = i;
                break;
            } if (queue.Contains(entry[i]))
            {
                while (queue.Peek() != entry[i])
                {
                    queue.Dequeue();
                }
                queue.Dequeue();
                queue.Enqueue(entry[i]); 
            } else 
            {
                queue.Enqueue(entry[i]);
            }
        }

        return lastIndex.ToString();
    }
}