﻿using System.Text;

namespace AdventOfCode2022.d05;

public class D05 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d05/", "input");

    public string R01()
    {
        var cratesArray = ExtractCrates();

        var lines          = File.ReadAllLines(Input);
        var firstLineIndex = lines.TakeWhile(l => l == "" || l.ToCharArray()[0] != 'm').Count();

        for (var i = firstLineIndex; i < lines.Length; i++)
        {
            var line = lines[i].Split(' ');

            var count = int.Parse(line[1]);
            var from  = int.Parse(line[3]) - 1;
            var to    = int.Parse(line[5]) - 1;

            for (var j = 0; j < count; j++)
            {
                cratesArray[to].Push(cratesArray[from].Pop());
            }
        }

        var sb = new StringBuilder();
        sb = cratesArray.Select(crate => crate.Peek()).Aggregate(sb, (current, ch) => current.Append(ch));
        return sb.ToString();
    }

    public string R02()
    {
        var cratesArray = ExtractCrates();

        var lines          = File.ReadAllLines(Input);
        var firstLineIndex = lines.TakeWhile(l => l == "" || l.ToCharArray()[0] != 'm').Count();

        for (var i = firstLineIndex; i < lines.Length; i++)
        {
            var line = lines[i].Split(' ');

            var count = int.Parse(line[1]);
            var from  = int.Parse(line[3]) - 1;
            var to    = int.Parse(line[5]) - 1;

            var tempStack = new Stack<char>();
            for (var j = 0; j < count; j++)
            {
                tempStack.Push(cratesArray[from].Pop());
            }
            while (tempStack.TryPop(out var pop))
            {
                cratesArray[to].Push(pop);
            }
        }

        var sb = new StringBuilder();
        sb = cratesArray.Select(crate => crate.Peek()).Aggregate(sb, (current, ch) => current.Append(ch));
        return sb.ToString();
    }

    private static Stack<char>[] ExtractCrates()
    {
        var lines = File.ReadAllLines(Input);

        var sizeOfPiles    = lines.TakeWhile(l => l[1] == ' ' || l[0] == '[').Count();
        var numberOfCrates = int.Parse(lines[sizeOfPiles].Trim().Split(' ').Last());
        var cratesArray    = new Stack<char>[numberOfCrates];

        for (var i = 0; i < numberOfCrates; i++)
        {
            cratesArray[i] = new Stack<char>();
        }


        for (var index = sizeOfPiles - 1; index >= 0; index--)
        {
            var line = lines[index];
            if (line[1] == '1')
            {
                break;
            }

            var stackId = 0;
            for (var c = 0; c < line.Length - 2; c += 4)
            {
                var elem = line.Substring(c, 3);
                if (elem.StartsWith("["))
                {
                    cratesArray[stackId].Push(elem.ToCharArray()[1]);
                }

                stackId++;
            }
        }

        return cratesArray;
    }
}