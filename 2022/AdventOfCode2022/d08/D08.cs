﻿namespace AdventOfCode2022.d08;

public class D08 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d08/", "input");

    public string R01()
    {
        var stringLines = File.ReadAllLines(Input);
        return stringLines.SelectMany((line, y) =>
                                          line.Select((c, x) => new { c, x, y })
                                              .Where(coord => coord.y == 0                                             || coord.x == 0 || coord.y == stringLines.Length - 1 || coord.x == line.Length - 1 ||
                                                              stringLines.Take(coord.y).All(l => l[coord.x] < coord.c) || stringLines.Skip(coord.y + 1).All(l => l[coord.x] < coord.c) ||
                                                              line.Take(coord.x).All(cc => cc               < coord.c) || line.Skip(coord.x        + 1).All(cc => cc        < coord.c))
                                              .Select(coord => 1))
            .Sum().ToString();
    }

    public string R02()
    {
        var stringLines    = File.ReadAllLines(Input);
        var maxScenicScore = 0;

        for (var y = 0; y < stringLines.Length; y++)
        {
            for (var x = 0; x < stringLines[0].Length; x++)
            {
                if (y == 0 || x == 0 || y == stringLines.Length - 1 || x == stringLines[0].Length - 1)
                {
                    //scenic score is mul 0
                    continue;
                }

                var tree         = (int)char.GetNumericValue(stringLines[y][x]);
                int scenicScoreN = 0, scenicScoreS = 0, scenicScoreO = 0, scenicScoreE = 0;
                for (var indexN = y - 1; indexN >= 0; indexN--)
                {
                    scenicScoreN++;
                    if ((int)char.GetNumericValue(stringLines[indexN][x]) >= tree)
                    {
                        break;
                    }
                }

                for (var indexS = y + 1; indexS < stringLines.Length; indexS++)
                {
                    scenicScoreS++;
                    if ((int)char.GetNumericValue(stringLines[indexS][x]) >= tree)
                    {
                        break;
                    }
                }


                for (var indexO = x - 1; indexO >= 0; indexO--)
                {
                    scenicScoreO++;
                    if ((int)char.GetNumericValue(stringLines[y][indexO]) >= tree)
                    {
                        break;
                    }
                }

                for (var indexE = x + 1; indexE < stringLines.Length; indexE++)
                {
                    scenicScoreE++;
                    if ((int)char.GetNumericValue(stringLines[y][indexE]) >= tree)
                    {
                        break;
                    }
                }

                maxScenicScore = Math.Max(maxScenicScore, scenicScoreN * scenicScoreS * scenicScoreE * scenicScoreO);
            }
        }

        return maxScenicScore.ToString();
    }
}