﻿namespace AdventOfCode2022.d09;

public class D09 : IExercise
{
    private static readonly string Input     = Path.Combine("./../../../d09/", "input");
    private static          string _example  = "R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20";
    private static          string _example1 = "R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2";

    private readonly Dictionary<string, (int, int)> _commandsHead = new()
    {
        { "R", (1, 0) },
        { "L", (-1, 0) },
        { "U", (0, 1) },
        { "D", (0, -1) }
    };

    public string R01()
    {
        return File.ReadAllLines(Input)
            .Select(l => l.Split(' '))
            .Aggregate((head: (x: 0, y: 0), tail: (x: 0, y: 0), visited: new HashSet<(int, int)>()), (coords, command) =>
            {
                for (var i = 0; i < int.Parse(command[1]); i++)
                {
                    var commandDelta = _commandsHead[command[0]];
                    coords.head.x += commandDelta.Item1;
                    coords.head.y += commandDelta.Item2;

                    var dX = coords.head.x - coords.tail.x;
                    var dY = coords.head.y - coords.tail.y;

                    if (Math.Abs(dX) > 1)
                    {
                        // tail should move
                        coords.tail.x += commandDelta.Item1;
                        if (Math.Abs(dY) == 1)
                        {
                            coords.tail.y += dY;
                        }
                    }
                    else if (Math.Abs(dY) > 1)
                    {
                        // tail should move
                        coords.tail.y += commandDelta.Item2;
                        if (Math.Abs(dX) == 1)
                        {
                            coords.tail.x += dX;
                        }
                    }

                    coords.visited.Add(coords.tail);
                }
                return coords;
            }, tuple => tuple.visited.Count).ToString();
    }

    public string R02()
    {
        return File.ReadAllLines(Input)
            .Select(l => l.Split(' '))
            .Aggregate((rope: new (int, int)[10], visited: new HashSet<(int, int)>()), (coords, command) =>
            {
                for (var i = 0; i < int.Parse(command[1]); i++)
                {
                    var commandDelta = _commandsHead[command[0]];
                    coords.rope[0].Item1 += commandDelta.Item1;
                    coords.rope[0].Item2 += commandDelta.Item2;

                    for (var k = 1; k < 10; k++)
                    {
                        var dX = coords.rope[k -1].Item1 - coords.rope[k].Item1;
                        var dY = coords.rope[k -1].Item2 - coords.rope[k].Item2;

                        if (Math.Abs(dX) + Math.Abs(dY) <= 1 || (Math.Abs(dX) == 1 && Math.Abs(dY) == 1))
                        {
                            // close, no changes
                        } else
                        {
                            if (Math.Abs(dX) == 2 && dY == 0)
                            {
                                coords.rope[k].Item1 += dX / 2;
                            } else if (Math.Abs(dY) == 2 && dX == 0)
                            {
                                coords.rope[k].Item2 += dY / 2;
                            } else if (Math.Abs(dX) > Math.Abs(dY))
                            {
                                coords.rope[k].Item1 += dX / 2;
                                coords.rope[k].Item2 += dY;
                            }else if (Math.Abs(dY) > Math.Abs(dX))
                            {
                                coords.rope[k].Item1 += dX;
                                coords.rope[k].Item2 += dY / 2;
                            } else if (Math.Abs(dX) == 2 && Math.Abs(dY) == 2)
                            {
                                coords.rope[k].Item1 += dX / 2;
                                coords.rope[k].Item2 += dY / 2;
                            }
                        }
                    }
                    
                    coords.visited.Add((coords.rope[9].Item1, coords.rope[9].Item2));
                }
                return coords;
            }, tuple => tuple.visited.Count).ToString();
    }
}