﻿using System.Text;

namespace AdventOfCode2022.d03;

public class D03 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d03/", "input");

    public string R01()
    {
        return File.ReadAllLines(Input)
            .Select(sack =>
            {
                var half1 = sack[..(sack.Length / 2)];
                var half2 = sack.Substring(sack.Length / 2, sack.Length / 2);
                var r     = Encoding.ASCII.GetBytes(sack.First(c => half1.Contains(c) && half2.Contains(c)).ToString())[0];
                return r > 96 ? r - 96 : 26 + r - 64;
            })
            .Sum()
            .ToString();
    }

    public string R02()
    {
        return File.ReadAllLines(Input)
            .Chunk(3)
            .Select(lines => { return lines[0].First(cl1 => lines[1].Contains(cl1) && lines[2].Contains(cl1)); })
            .Select(c =>
            {
                var r = Encoding.ASCII.GetBytes(c.ToString())[0];
                return r > 96 ? r - 96 : 26 + r - 64;
            })
            .Sum()
            .ToString();
    }
}