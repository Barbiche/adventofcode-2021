﻿namespace AdventOfCode2022.d07;

public class D07 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d07/", "input");

    private static string Example =
        "$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k";
    
    private static Folder? Parse(IEnumerable<string> commands)
    {
        Folder? currentFolder = null;
        foreach (var command in commands)
        {
            if (command.StartsWith('$'))
            {
                // Command handle
                var dollarCommand = command.Split(' ');

                if (dollarCommand[1] == "cd")
                {
                    // Changing current folder
                    if (dollarCommand[2] == "..")
                    {
                        // Going back to parent
                        currentFolder = currentFolder?.Parent;
                    }
                    else
                    {
                        var newFolder = new Folder(dollarCommand[2], currentFolder, new List<Folder>(), new List<File>());
                        currentFolder?.Folders.Add(newFolder);
                        currentFolder = newFolder;
                    }
                }
                // ls : we don't care.
            } else if (command.StartsWith("dir"))
            {
                // we don't care I guess
            }
            else
            {
                var file = ParseFile(command);
                currentFolder?.Files.Add(file);
            }
        }

        // Get back to parent
        while (currentFolder?.Parent != null)
        {
            currentFolder = currentFolder.Parent;
        }

        return currentFolder;
    }
    

    private static File ParseFile(string command)
    {
        var line = command.Split(' ');
        return new File(line[1], int.Parse(line[0]));
    }

    private static int GetFolderSize(Folder folder) => folder.Files.Sum(f => f.Size) + folder.Folders.Select(GetFolderSize).Sum();

    public string R01()
    {
        var allCommands = System.IO.File.ReadAllLines(Input);

        var rootFolder = Parse(allCommands);

        var allFoldersPlain = new HashSet<Folder>();
        GetAllFolders(rootFolder, in allFoldersPlain);

        return allFoldersPlain.Select(GetFolderSize).Where(s => s <= 100000).Sum().ToString();
    }

    private static void GetAllFolders(Folder folder, in HashSet<Folder> folders)
    {
        folders.Add(folder);
        foreach (var fold in folder.Folders)
        {
            GetAllFolders(fold, folders);
        }
    }

    private const int TotalSize  = 70000000;
    private const int NeededSize = 30000000;
    
    public string R02()
    {
        var allCommands = System.IO.File.ReadAllLines(Input);

        var rootFolder = Parse(allCommands);

        var allFoldersPlain = new HashSet<Folder>();
        GetAllFolders(rootFolder, in allFoldersPlain);

        var currentTotal = GetFolderSize(rootFolder);

        return allFoldersPlain.Select(GetFolderSize).OrderBy(i => i).First(s =>
        {
            return TotalSize - currentTotal + s >= NeededSize;
        }).ToString();
    }

    record File(string Name, int Size);

    record Folder(string Name, Folder? Parent, List<Folder> Folders, List<File> Files);
}