﻿using System.Diagnostics;
using AdventOfCode2022;
using AdventOfCode2022.d01;
using AdventOfCode2022.d02;
using AdventOfCode2022.d03;
using AdventOfCode2022.d04;
using AdventOfCode2022.d05;
using AdventOfCode2022.d06;
using AdventOfCode2022.d07;
using AdventOfCode2022.d08;
using AdventOfCode2022.d09;
using AdventOfCode2022.d10;
using AdventOfCode2022.d11;

var exercises = new IExercise[]
{
    new D01(),
    new D02(),
    new D03(),
    new D04(),
    new D05(),
    new D06(),
    new D07(),
    new D08(),
    new D09(),
    new D10(),
    new D11()
};

for (var i = 0; i < exercises.Length; i++)
{
    Console.WriteLine($"--- Day {i +1}");
    var watch = Stopwatch.StartNew();
    var r01   = exercises[i].R01();
    watch.Stop();
    Console.WriteLine($"'R01: {r01} -- {watch.Elapsed}");
    watch.Restart();
    var r02 = exercises[i].R02();
    watch.Stop();
    Console.WriteLine($"'R02: {r02} -- {watch.Elapsed}");
    Console.WriteLine("-------");
}

Console.ReadLine(); 