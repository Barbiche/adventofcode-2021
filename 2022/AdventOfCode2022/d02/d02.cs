﻿namespace AdventOfCode2022.d02;

public class D02 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d02/", "input");

    private static Dictionary<char, int> _values = new()
    {
        { 'X', 1 }, { 'Y', 2 }, { 'Z', 3 }
    };

    private static Dictionary<(char, char), int> _outcomes = new()
    {
        { ('A', 'X'), 3 },
        { ('A', 'Y'), 6 },
        { ('A', 'Z'), 0 },
        { ('B', 'X'), 0 },
        { ('B', 'Y'), 3 },
        { ('B', 'Z'), 6 },
        { ('C', 'X'), 6 },
        { ('C', 'Y'), 0 },
        { ('C', 'Z'), 3 }
    };
    
    private static Dictionary<(char, char), char> _outcomesr2 = new()
    {
        { ('A', 'X'), 'Z' },
        { ('A', 'Y'), 'X' },
        { ('A', 'Z'), 'Y' },
        { ('B', 'X'), 'X' },
        { ('B', 'Y'), 'Y' },
        { ('B', 'Z'), 'Z' },
        { ('C', 'X'), 'Y' },
        { ('C', 'Y'), 'Z' },
        { ('C', 'Z'), 'X' }
    };

    private static Dictionary<char, int> _winOutcomes = new()
    {
        { 'X', 0 }, { 'Y', 3 }, { 'Z', 6 }
    };

    public string R01()
    {
        return File.ReadAllLines(Input)
            .Select(l => l
                        .Split(" ")
                        .Select(char.Parse)
                        .ToArray())
            .Sum(line => _values[line[1]] + _outcomes[(line[0], line[1])])
            .ToString();
    }
    
    public string R02()
    {
        return File.ReadAllLines(Input)
            .Select(l => l
                        .Split(" ")
                        .Select(char.Parse)
                        .ToArray())
            .Sum(line => _values[_outcomesr2[(line[0], line[1])]] + _winOutcomes[line[1]])
            .ToString();
    }
}