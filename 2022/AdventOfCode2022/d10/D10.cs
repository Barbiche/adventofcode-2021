﻿namespace AdventOfCode2022.d10;

public class D10 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d10/", "input");

    private static string Example =
        "addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop";
    public string R01()
    {
        return File.ReadAllLines(Input)
            .Select(l => l.Split(' '))
            .Aggregate((x: 1, cycleValue: 0, strenghts: new List<int>()), (values, commands) =>
            {
                switch (commands[0])
                {
                    case "noop":
                    {
                        values.cycleValue++;
                    
                        if (values.cycleValue == 20 || (values.cycleValue - 20) % 40 == 0)
                        {
                            values.strenghts.Add(values.cycleValue * values.x);
                        }

                        break;
                    }
                    case "addx":
                    {
                        values.cycleValue++;

                        if (values.cycleValue == 20 || (values.cycleValue - 20) % 40 == 0)
                        {
                            values.strenghts.Add(values.cycleValue * values.x);
                        }
                    
                        values.cycleValue++;
                    
                        if (values.cycleValue == 20 || (values.cycleValue - 20) % 40 == 0)
                        {
                            values.strenghts.Add(values.cycleValue * values.x);
                        }
                    
                        values.x += int.Parse(commands[1]);
                        break;
                    }
                    default:
                        return values;
                }

                return values;
            }, tuple => tuple.strenghts.Sum()).ToString();
    }

    public string R02()
    {
        var image =  File.ReadAllLines(Input)
            .Select(l => l.Split(' '))
            .Aggregate((x: 1, cycleValue: 0, image: new char[240]), (values, commands) =>
            {
                switch (commands[0])
                {
                    case "noop":
                    {
                        values.image[values.cycleValue] = values.cycleValue % 40 >= values.x - 1 && values.cycleValue % 40 <= values.x + 1 ? '#' : '.'; 
                        values.cycleValue++;
                        break;
                    }
                    case "addx":
                    {
                        values.image[values.cycleValue] = values.cycleValue % 40 >= values.x - 1 && values.cycleValue % 40 <= values.x + 1 ? '#' : '.'; 
                        values.cycleValue++;

                        values.image[values.cycleValue] = values.cycleValue % 40 >= values.x - 1 && values.cycleValue % 40 <= values.x + 1 ? '#' : '.'; 
                        values.cycleValue++;

                        values.x += int.Parse(commands[1]);
                        break;
                    }
                    default:
                        return values;
                }

                return values;
            }, tuple =>new string(tuple.image));

        for (var i = 0; i < 6; i++)
        {
            Console.WriteLine(image.Substring(i*40, 40));
        }

        return "";
    }
}