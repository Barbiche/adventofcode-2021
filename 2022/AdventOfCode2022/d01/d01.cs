﻿namespace AdventOfCode2022.d01;

public class D01 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d01/", "input");

    public string R01() =>
        File.ReadAllText(Input)
            .Split("\n\n")
            .Select(cal => cal.Split("\n")
                        .Select(int.Parse)
                        .Sum())
            .MaxBy(cal => cal)
            .ToString();

    public string R02() =>
        File.ReadAllText(Input)
            .Split("\n\n")
            .Select(cal => cal.Split("\n")
                        .Select(int.Parse)
                        .Sum())
            .OrderByDescending(cal => cal)
            .Take(3)
            .Sum()
            .ToString();
}