﻿namespace AdventOfCode2022.d11;

public class D11 : IExercise
{
    private const string Example =
        "Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1";

    private static readonly string Input = Path.Combine("./../../../d11/", "input");

    public string R01()
    {
        _divisorLimit = 1;
        // var monkeyStrings = File.ReadAllText(Input).Split("\n\n");
        var monkeyStrings = Example.Split("\n\n");


        var monkeys = monkeyStrings.Select(ms => ParseMonkey(ms.Split('\n'))).ToDictionary(monkey => monkey.Id, monkey => monkey);

        var nbOfInspections = monkeys.ToDictionary(monkey => monkey.Key, _ => 0);
        for (var i = 0; i < 20; i++)
        {
            foreach (var monkeyPair in monkeys)
            {
                foreach (var newItem in monkeyPair.Value.Items.Select(monkeyItem => monkeyPair.Value.Operation(monkeyItem) / 3))
                {
                    nbOfInspections[monkeyPair.Key]++;
                    if (monkeyPair.Value.Test(newItem))
                    {
                        monkeys[monkeyPair.Value.MonkeyIdTrue].Items.Add(newItem);
                    }
                    else
                    {
                        monkeys[monkeyPair.Value.MonkeyIdFalse].Items.Add(newItem);
                    }
                }
                monkeyPair.Value.Items.Clear();
            }
        }

        return nbOfInspections.OrderByDescending(pair => pair.Value).Take(2).Aggregate(1, (mul, pair) => pair.Value * mul, mul => mul).ToString();
    }

    public string R02()
    {
        _divisorLimit = 1;
        var monkeyStrings = File.ReadAllText(Input).Split("\n\n");

        var monkeys = monkeyStrings.Select(ms => ParseMonkey(ms.Split('\n'))).ToDictionary(monkey => monkey.Id, monkey => monkey);

        var nbOfInspections = monkeys.ToDictionary(monkey => monkey.Key, _ => (long)0);
        for (var i = 0; i < 10000; i++)
        {
            foreach (var monkeyPair in monkeys)
            {
                foreach (var newItem in monkeyPair.Value.Items.Select(monkeyItem =>
                         {
                             var value = monkeyPair.Value.Operation(monkeyItem);
                             return value % _divisorLimit;
                         }))
                {
                    nbOfInspections[monkeyPair.Key]++;
                    if (monkeyPair.Value.Test(newItem))
                    {
                        monkeys[monkeyPair.Value.MonkeyIdTrue].Items.Add(newItem);
                    }
                    else
                    {
                        monkeys[monkeyPair.Value.MonkeyIdFalse].Items.Add(newItem);
                    }
                }
                monkeyPair.Value.Items.Clear();
            }
        }

        return nbOfInspections.OrderByDescending(pair => pair.Value).Take(2).Select(pair => pair.Value).Aggregate((m1, m2) => m1 * m2).ToString();
    }

    private static long _divisorLimit = 1;
    
    private static Monkey ParseMonkey(IReadOnlyList<string> monkeyString)
    {
        var monkeyId = int.Parse(monkeyString[0].Split(' ')[1].Trim(':'));

        var items = monkeyString[1].Split(' ').Skip(4).Select(item => item.Trim(',')).Select(long.Parse).ToList();

        var operationStrings = monkeyString[2].Split(' ').Skip(3);
        var operation        = CreateOperation(operationStrings.ToArray());

        var divisor = long.Parse(monkeyString[3].Split(' ').Last());
        _divisorLimit *= divisor;
        var test    = new Func<long, bool>(value => value % divisor == 0);

        var monkeyTrueId  = int.Parse(monkeyString[4].Split(' ').Last());
        var monkeyFalseId = int.Parse(monkeyString[5].Split(' ').Last());
        
        return new Monkey(monkeyId, items, operation, test, monkeyTrueId, monkeyFalseId);
    }

    private static Func<long, long> CreateOperation(IReadOnlyList<string> operation)
    {
        return operation[3] switch
        {
            "+" => old => old + (operation[4] == "old" ? old : long.Parse(operation[4])),
            "*" => old => old * (operation[4] == "old" ? old : long.Parse(operation[4])),
            _   => throw new NotSupportedException()
        };
    }

    private record Monkey(int Id, List<long> Items, Func<long, long> Operation, Func<long, bool> Test, int MonkeyIdTrue, int MonkeyIdFalse);
}