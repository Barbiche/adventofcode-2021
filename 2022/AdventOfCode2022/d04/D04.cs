﻿namespace AdventOfCode2022.d04;

public class D04 : IExercise
{
    private static readonly string Input = Path.Combine("./../../../d04/", "input");

    public string R01() =>
        File.ReadAllLines(Input)
            .Where(l =>
            {
                var pairs = l.Split(',');
                var pair1 = pairs[0].Split('-').Select(int.Parse).ToArray();
                var pair2 = pairs[1].Split('-').Select(int.Parse).ToArray();
                return pair1[0]    >= pair2[0] && pair1[1] <= pair2[1]
                       || pair2[0] >= pair1[0] && pair2[1] <= pair1[1];
            }).Count().ToString();

    public string R02() =>
        File.ReadAllLines(Input)
            .Where(l =>
            {
                var pairs = l.Split(',');
                var pair1 = pairs[0].Split('-').Select(int.Parse).ToArray();
                var pair2 = pairs[1].Split('-').Select(int.Parse).ToArray();
                return (pair1[1] >= pair2[0] && pair1[1] <= pair2[1]) || (pair2[1] >= pair1[0] && pair2[1] <= pair1[1]);
            }).Count().ToString();
}