fn main() {
    let input: &str = include_str!("..\\input.txt");
    let positions: Vec<i128> = input.replace('\n', "")
                                    .split(',')
                                    .map(|x| x.parse::<i128>().unwrap())
                                    .collect::<Vec<i128>>();

    let min: i128 = positions.iter().min().unwrap().clone();
    let max: i128 = positions.iter().max().unwrap().clone() + 1;
    let min      = (min..max)
                    .map(|d| {
                        let val: i128 = positions.iter().map(|p| {
                            let fact : i128 = (0..(p - d).abs()+1).sum();
                            fact
                        }).sum();
                        val
                    })
                    .min()
                    .unwrap();

    println!("{}", min);
}
