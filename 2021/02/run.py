def main():
    with open('input.txt', 'r') as f:
        hor = 0
        depth = 0
        aim = 0
        for line in f:
            splitted = line.split(' ')
            full_command = {"commmand": splitted[0], "value": int(splitted[1])}
            print(full_command["value"])
            match full_command["commmand"]:
                case "forward":
                    hor += full_command["value"]
                    depth += aim * full_command["value"]
                case "down":
                    aim += full_command["value"]
                case "up":
                    aim -= full_command["value"]
    
    return hor * depth

if __name__ == '__main__':
    print(main())