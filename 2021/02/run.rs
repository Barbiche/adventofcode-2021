pub fn main()
{   
    let mut hor: u32 = 0;
    let mut depth: u32 = 0;
    let mut aim: u32 = 0;

    for l in include_str!("input.txt")
                .lines()
                .map(|l| l.split_once(" ").unwrap())
                .map(|(a,b)| (a, b.parse::<u32>().unwrap()))
    {
        match l {
            ("forward", value) => {
                hor += value;
                depth += aim * value
            },
            ("down", value) => aim += value,
            ("up", value) => aim -= value,
            _ => unreachable!(),
        }
    }

    println!("{}", hor * depth);

}