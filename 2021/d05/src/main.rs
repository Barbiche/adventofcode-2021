use std::fmt;
use std::collections::HashMap;
use std::cmp;

struct Line
{
    x1: i32,
    y1: i32,
    x2: i32,
    y2: i32
}

#[derive(Hash)]
struct Point
{
    x: i32,
    y: i32
}

impl fmt::Display for Line {
    fn fmt(&self, l: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> { 
        write! (l, "({}, {}), ({}, {})", self.x1, self.y1, self.x2, self.y2)
    }
}

impl std::cmp::PartialEq for Point { 
    fn eq(&self, other: &Point) -> bool { 
        self.x == other.x && self.y == other.y
     }
}
impl Eq for Point {}

impl fmt::Display for Point {
    fn fmt(&self, l: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> { 
        write! (l, "({}, {})", self.x, self.y)
    }
}

pub fn main()
{   
    let input: &str = include_str!("..\\input.txt");
    let points = input.lines()
         .map(|l: &str| {
             l.split_once(" -> ")
              .map(|(d1, d2)| {
                  Line { 
                    x1: d1.split_once(",").unwrap().0.parse::<i32>().unwrap(),
                    y1: d1.split_once(",").unwrap().1.parse::<i32>().unwrap(),
                    x2: d2.split_once(",").unwrap().0.parse::<i32>().unwrap(),
                    y2: d2.split_once(",").unwrap().1.parse::<i32>().unwrap()
                  }
              })
              .unwrap()
         })
         .fold(Vec::new(), |mut points: Vec<Point>, l: Line| {
            let dx = if l.x1 == l.x2 {0} else {(l.x2 - l.x1) / (l.x2 - l.x1).abs()};
            let dy = if l.y1 == l.y2 {0} else {(l.y2 - l.y1) / (l.y2 - l.y1).abs()};
            let mut x = l.x1;
            let mut y = l.y1;
            while x != l.x2 || y != l.y2
            {
                points.push(Point{x:x, y:y});
                x += dx;
                y +=dy;
            } 
            points.push(Point{x:l.x2, y:l.y2});
            points
         }).into_iter()
         .fold(HashMap::new(), |mut occurences: HashMap<Point, i32>, p| {
            *occurences.entry(p).or_insert(0)+= 1;
            occurences
         })
         .into_iter()
         .filter(|(_,v)| *v >= 2)
         .count();
        
    println!("{}", points);
}