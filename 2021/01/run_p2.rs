pub fn main()
{   
    let lines = include_str!("input.txt")
                    .lines()
                    .map(|l| l.parse().unwrap())
                    .collect::<Vec<u16>>();
    
    let mut count = 0;
    for l in 3..lines.len()
    {
        if lines[l] + lines[l-1] + lines[l-2] > lines[l-1] + lines[l-2] + lines[l-3]
        {
            count = count + 1;
        }
    }

    println!("{}", count);

}