
def main():
    with open('input.txt', 'r') as f:
        lines = f.readlines()
    
    count = 0
    for l in range(3, len(lines)):
        if (int(lines[l]) + int(lines[l-1]) + int(lines[l-2])) > (int(lines[l-1]) + int(lines[l-2]) + int(lines[l-3])):
            count = count + 1
    
    return count


if __name__ == '__main__':
    print(main())