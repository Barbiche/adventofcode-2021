pub fn main()
{   
    let lines = include_str!("input.txt")
                    .lines()
                    .map(|l| l.parse().unwrap())
                    .collect::<Vec<u16>>();
    
    let mut count = 0;
    for l in 1..lines.len()
    {
        if lines[l] > lines[l-1]
        {
            count = count + 1;
        }
    }

    println!("{}", count);

}