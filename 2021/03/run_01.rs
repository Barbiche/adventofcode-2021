pub fn main()
{   
    let lines = include_str!("input.txt").lines().collect::<Vec<&str>>();
    let nb_of_lines = lines.len();
    let line_size = lines[0].len();

    let gamma = lines.iter()
                    .map(|l| usize::from_str_radix(l, 2).unwrap())
                    .fold(vec![0; line_size], |counts, line_bits| {
                        counts
                        .into_iter()
                        .enumerate()
                        .map(|(i, n)| n + ((line_bits & 1 << i) >> i))
                        .collect()
                    })
                    .into_iter()
                    .enumerate()
                    .map(|(i, b)| ((b >= nb_of_lines / 2) as u32) << i)
                    .sum::<u32>();
            
    println!("{}", gamma * (!gamma & ((1 << line_size) - 1)));
}